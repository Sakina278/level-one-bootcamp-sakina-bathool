//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input1(){
   int num1;
   printf("Enter the number\n");
   scanf("%d",&num1);
   return num1;
}
int input2(){
    int num2;
    printf("Enter the number\n");
    scanf("%d",&num2);
    return num2;
}
int add(int a,int b)
{  int sum;
   sum = a+b;
   return sum;
}
void output(int sum)
{
printf("The sum is %d",sum);
}
int main()
 {  int p,q,r;
   p=input1();
   q=input2();
   r=add(p,q);
   output(r);
return 0;
}
