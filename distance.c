//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float a(){
     float x;
     scanf("%f",&x);
     return x;
}
float b(){
    float y;
    scanf("%f",&y);
    return y;
}
float compute(float x1,float y1,float x2,float y2){
    float distance =  ( (x2-x1)*(x2-x1))+((y2-y1)*(y2-y1) );
    float dist = sqrt(distance);
    return dist;
}
void output(float dis){
    printf("The distance is %f\n",dis);
}
int main(){
     float x1,y1,x2,y2, res;
     printf("Enter the value of x1:");
     x1=a();

     printf("Enter the value of y1:");
     y1=b();

    printf("Enter the value of x2:");
    x2=a();

    printf("Enter the value of y2:");
    y2=b();
    
    res = compute(x1,y1,x2,y2);
    output(res);
}
