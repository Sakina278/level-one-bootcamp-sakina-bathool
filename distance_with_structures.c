//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct point{
    float x,y;
}s,r;

float a(){
float x;
scanf("%f",&x);
return x;
}

float b(){
float y;
scanf("%f",&y);
return y;
}


float compute(struct point s,struct point r)
{
float distance = ((s.x-r.x)*(s.x-r.x))+((s.y-r.y)*(s.y-r.y));
float dist = sqrt(distance);
return dist;
}
void output(float dis){
printf("The distance is: %f",dis);
}
int main(){
printf("Enter x1:");
scanf("%f",&s.x);
printf("Enter y1:");
scanf("%f",&s.y);
printf("Enter x2:");
scanf("%f",&r.x);
printf("Enter y2:");
scanf("%f",&r.y);
float dist = compute(s,r);
output(dist);
return 0;
}
