//WAP to find the sum of n fractions.
#include<stdio.h>
#include<math.h>
struct fraction{
    int n;
    int d;
} ;
typedef struct fraction frac;

frac input() {
   frac f;
   printf("Enter numerator");
   scanf("%d", &f.n);
   printf("Enter denominator");
   scanf("%d", &f.d);
   return f;
}

int gcd(int x,int y)
{
    int i,z;

    for(i=x<y?x:y; i >= 1;i--)
    {
        if(x%i==0 && y%i==0)
          {
            z = i;
            break;
           }
    }
    return z;
    }

int find_numerator(int lcm,int n1,int d1,int n2,int d2)
{
     int sum= (lcm/d1)*n1+(lcm/d2)*n2;
    return sum;
}
 
frac compute(frac f1,frac f2)
{
    frac f3;
    int r;
    f3.d = f1.d*f2.d/gcd(f1.d,f2.d);
    f3.n = find_numerator(f3.d,f1.n,f1.d,f2.n,f2.d);
    r = gcd(f3.n,f3.d);
    f3.n = f3.n /r;
    f3.d = f3.d /r;
    return f3;
}
int output(frac s)
{
    printf("The sum of given fractions is %d/%d\n",s.n,s.d);
    return 0;
}

int main () {
  int  n,i;
  frac p,q;
  printf("Enter the no. of fractions:");
  scanf("%d",&n);
  frac f[n];
  for(i=0;i<n;i++)
  {
     printf("Enter the fraction %d\n",i+1);
     f[i] = input();
  }
  if(n==1)
  {
    q = f[0];
  }
 else
 {
  p = f[0];
  for(i=0;i<n-1;i++)
  {
     q = compute(p,f[i+1]);
     p = q;
  }
}
output(q);
return 0;
}
