//Write a program to find the sum of n different numbers using 4 functions
#include <stdio.h>
 
int main()
{
  int n, sum = 0, i, num;
 
  printf("Enter the value of n: \n");
  scanf("%d", &n);
 
 printf("Enter %d numbers to be added: \n", n);
 
  for (i = 1; i <= n; i++)
  {
    scanf("%d", &num);
    sum = sum + num;
  }
 
  printf("The sum of %d different numbers is %d\n", n, sum);
 
  return 0;
}
