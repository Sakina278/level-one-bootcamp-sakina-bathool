//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>

int input(){
   float x;
   scanf("%f",&x);
   return x;
}

float volume(int h,int d,int b){
     float vol;
     vol = 1.0/3.0*((h*d*b)+(d/b));
     return vol;
}

void output(float vol){
    printf("The volume of thromboloid is %f",vol );
}
int main() { 
    float a,b, c, d;
    printf("Enter the height of thromboloid:");
    a = input();
    printf("Enter the depth of thromboloid:");
    b = input();
    printf("Enter the breadth of thromboloid:");
    c = input();
    d = volume(a,b,c);
    output(d);
}
