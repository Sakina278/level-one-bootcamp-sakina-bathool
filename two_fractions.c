//WAP to find the sum of two fractions.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct fraction
{
     int n;
     int d;
};
typedef struct fraction frac;

frac input()
{
     frac f;
     printf("Enter numerator:");
     scanf("%d",&f.n);
     printf("Enter denominator:");
     scanf("%d",&f.d);
     return f;
}


int gcd(int x, int y)
{
    int i,z;
    for(i = x<y?x:y; i >=1 ; i--)
    {
        if(x % i == 0 && y % i == 0)
        {
               z = i;
               break;
         }
     }
return z;
}

int find_numerator(int lcm,int n1,int d1,int n2,int d2)
{
     int sum = (lcm/d1)*n1+(lcm/d2)*n2;
     return sum;
}


frac compute(frac f1, frac f2)
{
    frac f3;
    int r;
    f3.d = f1.d*f2.d/gcd(f1.d,f2.d);
    f3.n = find_numerator(f3.d,f1.n,f1.d,f2.n,f2.d);
    r = gcd(f3.n,f3.d);
    f3.n = f3.n/r;
    f3.d = f3.d/r;
    return f3;
}

int output(frac f1,frac f2,frac f3)
{
    printf("%d / %d + %d / %d is %d / %d\n",f1.n,f1.d,f2.n,f2.d,f3.n,f3.d);
    return 0;
}

int main()
{
    frac fr1,fr2,fr3;
    printf("enter the values of first fraction\n");
    fr1 = input();
    printf("enter the values of second fraction\n");
    fr2 = input();
    fr3 = compute(fr1,fr2);
    output(fr1,fr2,fr3);
    return 0;
}
